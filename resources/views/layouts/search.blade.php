<form method="POST" action="{{route('search')}}">
    @csrf
    <div class="container">
        <div class="row">
            <div class="col-3">
                <label for="title">Title</label>
                <input type="text" name="title">
            </div>
            <div class="col-3">
                <label for="year">Year</label>
                <input type="number" min="1890" max="2021">
            </div>
            <div class="col-3">
                <label for="type">Type</label>
                <select name="type" id="">
                    <option value="movie">Movie</option>
                    <option value="series">Series</option>
                    <option value="episode">Episode</option>
                </select>
            </div>
            <div class="col-6">
                <button class="btn-primary" type="submit">Search</button>
            </div>
        </div>
    </div>
</form>
