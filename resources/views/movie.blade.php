@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-4 offset-4">
                <img src="{{$movie->Poster}}" alt="">
            </div>
        </div>
        <div class="row">
            <div class="col-4 offset-4">
                <h3 class="">{{$movie->Title}}</h3>
            </div>

            <div class="col-6">
                <h4>Year: {{$movie->Year}}</h4>
            </div>
            <div class="col-6">
                <div class="text-left">
                    <h4 class="text-left">Rated: {{$movie->Rated}}</h4>
                </div>
            </div>
            <div class="col-6">
                <h4>Released: {{$movie->Released}}</h4>
            </div>
            <div class="col-6">
                <div class="text-left">
                    <h4>Genre: {{$movie->Genre}}</h4>
                </div>
            </div>
            <div class="col-6">
                <h4>Director: {{$movie->Director}}</h4>
            </div>
            <div class="col-6">
                <div class="text-left">
                    <h4>Writer: {{$movie->Writer}}</h4>
                </div>
            </div>

            <div class="col-6">
                <h4>Language: {{$movie->Language}}</h4>
            </div>
            <div class="col-6">
                <div class="text-left">
                    <h4>Country: {{$movie->Country}}</h4>
                </div>
            </div>
            <div class="col-6">
                <h4>Awards: {{$movie->Awards}}</h4>
            </div>
            <div class="col-6">
                <div class="text-left">
                    <h4>Actors: {{$movie->Actors}}</h4>
                </div>
            </div>
            <div class="col-6">
                <h4>Plot: {{$movie->Plot}}</h4>
            </div>

        </div>
    </div>
@endsection
