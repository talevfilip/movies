@extends('layouts.app')

@section('content')
    @include('layouts.search')
    <div class="container" style="margin-top: 69px">
        <table class="align-items-center">
            <thead class="thead-light">
            <tr>
                <th class="text-center pr-2">
                    Poster
                </th>
                <th class="text-center pr-2">
                    Title
                </th>
                <th class="text-center pr-2">
                    Year
                </th>
                <th class="text-center pr-2">
                    Type
                </th>
            </tr>
            </thead>
            <tbody class="list">
            @foreach($movies as $movie)

                <tr>
                    <td class="text-center pr-2">
                        <a href="{{route('imdb', $movie->imdbID)}}">
                            <img src="{{$movie->Poster}}" alt="" style="width: 120px; height: 120px;">
                        </a>
                    </td>
                    <td class="text-center pr-2">
                        <a href="{{route('imdb', $movie->imdbID)}}">
                            {{$movie->Title}} </a>
                    </td>
                    <td class="text-center pr-2">
                        <a href="{{route('imdb', $movie->imdbID)}}">
                            {{$movie->Year}}
                        </a></td>
                    <td class="text-center pr-2">
                        <a href="{{route('imdb', $movie->imdbID)}}">
                            {{$movie->Type}}
                        </a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection
