## Search Movies/Series

* **Introduction**

  `Search for Movies and series`


* **Description**

  `You can search all the movies and series in the world`


* **URL**
    - `/api/movies/search`


* **Url parameters**
    * **Required:** <br />
      `title` = [string] <br />

* **Optional:** <br />
  `year` = [integer] <br />
  `type` = [string] (movie, series) <br />


* **Method:**

  `GET`

* **Authentication:**

  `Header` =>

  `Authorization` = [How to get token](docs/api/token.md)


* **Success response:**

    * **Code:** 200 SUCCESS <br />
    * **Content:**
    ``` 
        {
           "movies": [
               {
                    "Title": "[string]",
                    "Year": "[integer]",
                    "imdbID": "[string]",
                    "Type": "[string]",
                    "Poster": "[string]"
               }
        }
    ```
 
