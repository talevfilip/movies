## Token

* **Introduction**

  `Get a valid token.`


* **Description**

  `A way to get a token for authorisation.`


* **URL**
    - `/api/login/`


* **Url parameters**
    * **Required:** <br />
      `email` = [string] <br />
      `password` = [string] <br />


* **Method:**

  `POST`

* **Authentication:**

  `Header` =>


* **Success response:**
    * **Content:**
    ``` 
        {
            "token" : "[string]"
        }
    ```
 
