<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function search(Request $request)
    {
        $title = $request['title'];
        $year = $request['year'];
        $type = $request['type'];
        $apiKey = '3ed2be74';

        $url = 'http://www.omdbapi.com/?apikey=' . $apiKey .'&s=' . $title . '&type=' . $type . '&year=' . $year;

        $client = new Client();
        $request = $client->get($url);
        $response = $request->getBody();

        $movies = json_decode($response->getContents());

        if($movies->Response == 'False') {
            abort(404);
        }

        $movies = $movies->Search;

        return view('searches', [
            'movies' => $movies
        ]);
    }

    public function imdb ($id)
    {
        $url = 'http://www.omdbapi.com/?apikey=3ed2be74&i=' . $id;
        $client = new Client();
        $request = $client->get($url);
        $response = $request->getBody();

        $movie = json_decode($response->getContents());

        return view('movie', [
            'movie'=>$movie
        ]);
    }
}
