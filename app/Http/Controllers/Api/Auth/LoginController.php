<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function login (Request $request)
    {
        $credentials = $request->only(['email', 'password']);

        $token = auth()->attempt($credentials);

        if(!$token) {
            return response()->json([
                'error' => 'You entered incorrect email or password',
                401
            ]);
        }

        return response()->json([
            'token' => $token
        ]);
    }
}
