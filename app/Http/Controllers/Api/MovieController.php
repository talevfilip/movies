<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Controller;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;

class MovieController extends Controller
{
    public function search(Request $request)
    {
        try {
            $user = auth()->userOrFail();
        } catch (JWTException $e) {
            return response()->json([
                'error' => $e->getMessage()
            ]);
        }

        $title = $request['title'];
        $year = $request['year'];
        $type = $request['type'];
        $apiKey = '3ed2be74';

        $url = 'http://www.omdbapi.com/?apikey=' . $apiKey . '&s=' . $title . '&type=' . $type . '&year=' . $year;

        $client = new Client();
        $request = $client->get($url);
        $response = $request->getBody();

        $movies = json_decode($response->getContents());

        if ($movies->Response == 'False') {
            abort(404);
        }

        $movies = $movies->Search;

        return response()->json([
            'movies' => $movies
        ]);
    }

    public function imdb ($id)
    {
        try {
            $user = auth()->userOrFail();
        } catch (JWTException $e) {
            return response()->json([
                'error' => $e->getMessage()
            ]);
        }

        $url = 'http://www.omdbapi.com/?apikey=3ed2be74&i=' . $id;
        $client = new Client();
        $request = $client->get($url);
        $response = $request->getBody();

        $movie = json_decode($response->getContents());

        return response()->json([
            'movies' => $movie
        ]);
    }
}
