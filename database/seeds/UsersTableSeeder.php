<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = $this->createUsers();
    }

    private function createUsers()
    {
        $user = new User([
            'name' => 'Test Test',
            'email' => 'test@test.com',
            'password' => Hash::make('123123123')
        ]);

        $user->save();

        return $user;
    }
}
