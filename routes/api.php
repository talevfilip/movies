<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', [
    'as' => 'login.login',
    'uses' => 'Api\Auth\LoginController@login'
]);

Route::get('/movies/search', [
    'as' => 'movies.search',
    'uses'=> 'Api\MovieController@search'
]);

Route::get('/movies/imdb/{id}', [
    'as' => 'movies.imdb',
    'uses'=> 'Api\MovieController@imdb'
]);
